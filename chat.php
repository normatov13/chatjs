<?php session_start(); 
if(!isset($_SESSION['id'])){
    header("Location:login.php");
}
?>

<?php  include_once "header.php" ; ?>
<body>
    <div class="wrapper">
       <section class="chat-area">
      <?php 
 require_once("php/config.php");
 $user_id = mysqli_real_escape_string($conn,$_GET['user_id']);
 $sql = mysqli_query($conn,"SELECT * FROM users WHERE id='{$user_id}'");
 if(mysqli_num_rows($sql)>0){
 $row = mysqli_fetch_assoc($sql);
//    print_r($row);
//    die();
 }else{
    header("Location:login.php");
 }
?>
<header>
   <a href="User.php" class="back-icon"><i class="fa fa-arrow-left"></i></a>
        <img src="php/images/<?php echo  trim(htmlspecialchars($row['img']))?>" alt="">
        <div class="details">
            <span><?php echo trim(htmlspecialchars($row['name']))." ".trim(htmlspecialchars($row['lname'])) ?></span>
            <p><?php echo  trim(htmlspecialchars($row['status'])) ?></p>
        </div>
    
</header>
<div class="chat-box">
   
</div>
  <form action="#" class="typing-area">
  <input type="hidden" name="outgoing_id"  value="<?php echo $_SESSION['id']; ?>">
   <input type="hidden" name="incoming_id"  value="<?php echo $user_id; ?>">
      <input type="text" name="message" class="input-field" placeholder="type message here...">
      <button><i class="fa fa-telegram-plane"></i></button>
  </form>
       </section>
    </div>
    <script src="javascript/chat.js"></script>
</body>

</html>