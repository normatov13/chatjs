const form = document.querySelector(".signup form"),
    continueBtn = form.querySelector(".submit input"),
    errText = form.querySelector(".error-txt");

form.onsubmit = (e) => {
    e.preventDefault();
}

continueBtn.onclick = () => {
    // Ajax request jo'natamiz !;
    let xhr = new XMLHttpRequest() // XML object yaratildi
    xhr.open("POST", "php/signup.php", true);

    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let data = xhr.response;
                if (data == "success") {
                    location.href = "/User.php";
                } else {
                    errText.textContent = data;
                    errText.style.display = "block";
                }
            }
        }
    }
    let Formdata = new FormData(form);
    xhr.send(Formdata);
}