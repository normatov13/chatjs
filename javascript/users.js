const searchBar = document.querySelector(".users .search input")
const searchBtn = document.querySelector(".users .search button");
const UsersList = document.querySelector(".users .users-list");

searchBtn.onclick = () => {
    searchBar.classList.toggle("active")
    searchBar.focus();
    searchBtn.classList.toggle("active")
    searchBar.value = '';
}
searchBar.onkeyup = () => {
    let searchTerm = searchBar.value;
    if (searchTerm != "") { // searchBar bo'sh bolmasa
        searchBar.classList.add("active"); // pastdagi setInterval funksiyasi avtomatik ishlab filterlangan datani o'zgartirib ybormasligi uchun yangi class qo'shilyapti
    } else {
        searchBar.classList.remove("active"); // agar bo'sh bolsa class o'chirib tashlanmoqda va setInterval funksiyasi umumiy ro'yhatni ko'rsatishni boshlaydi
    }
    // Ajax request jo'natamiz ;
    let xhr = new XMLHttpRequest() // XML object yaratildi !;
    xhr.open("POST", "php/search.php", true);
    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let data = xhr.response;
                UsersList.innerHTML = data;
            }
        }
    }
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("searchTerm=" + searchTerm);

}

setInterval(() => {
        // Ajax request jo'natamiz ;
        let xhr = new XMLHttpRequest() // XML object yaratildi !;
        xhr.open("GET", "php/users.php", true);
        xhr.onload = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    let data = xhr.response;
                    if (!searchBar.classList.contains("active")) { // Agar searchBar classList larida active classi bo'lsmasa umumiy ro'yhat tuziladi
                        UsersList.innerHTML = data;
                    }
                }
            }
        }
        xhr.send();
    }, 500) // Bu funktsiya har 500 ms dan keyin yangilanib turadi