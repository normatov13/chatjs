const form = document.querySelector(".typing-area"),
    inputField = form.querySelector(".input-field"),
    sendBtn = form.querySelector("button"),
    messageArea = document.querySelector(".chat-box");

form.onsubmit = (e) => {
    e.preventDefault();
}

sendBtn.onclick = () => {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "php/insert-chat.php", true);
    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                inputField.value = ''; // Input messageda hammasi goyida bolsa input boshatiladi !
            }
        }
    }
    let formData = new FormData(form);
    xhr.send(formData);
}

messageArea.onmouseenter = () => {
    messageArea.classList.add("active");
}
messageArea.onmauseleave = () => {
    messageArea.classList.remove("active");
}

setInterval(() => {
    // Ajax request jo'natamiz ;
    let xhr = new XMLHttpRequest() // XML object yaratildi !;
    xhr.open("POST", "php/getMessage.php", true);
    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let data = xhr.response;
                messageArea.innerHTML = data;
                if (!messageArea.classList.contains("active")) {
                    scroll();
                }
            }
        }
    }
    let formData = new FormData(form);
    xhr.send(formData);
}, 500);

function scroll() {
    messageArea.scrollTop = messageArea.scrollHeight;
}