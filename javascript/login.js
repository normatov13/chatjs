const form = document.querySelector(".login form"),
    continueBtn = form.querySelector(".submit input"),
    errText = form.querySelector(".error-txt");

form.onsubmit = (e) => {
    e.preventDefault();
}

continueBtn.onclick = () => {
    // Ajax request jo'natamiz !;
    let xhr = new XMLHttpRequest() // XML object yaratildi
    xhr.open("POST", "php/login.php", true);

    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let data = xhr.response;
                console.log(data);
                if (data == "success") {
                    location.href = "/User.php";
                } else {
                    errText.textContent = data;
                    errText.style.display = "block";
                }
            }
        }
    }
    let Formdata = new FormData(form);
    xhr.send(Formdata);
}