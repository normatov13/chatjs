<?php session_start(); 
if(!isset($_SESSION['id'])){
    header("Location:login.php");
}
?>

<?php  include_once "header.php" ; ?>
<body>
    <div class="wrapper">
       <section class="users">
<header>
<?php 
 require_once("php/config.php");
 $sql = mysqli_query($conn,"SELECT * FROM users WHERE id='{$_SESSION['id']}'");
if(mysqli_num_rows($sql)>0){
    $row = mysqli_fetch_assoc($sql);
//    print_r($row);
//    die();
}else{
    header("Location:login.php");
}
?>

    <div class="content">
        <img src="php/images/<?php echo  trim(htmlspecialchars($row['img']))?>" alt="img">
        <div class="details">
            <span><?php echo trim(htmlspecialchars($row['name']))." ".trim(htmlspecialchars($row['lname'])) ?></span>
            <p><?php echo  trim(htmlspecialchars($row['status'])) ?></p>
        </div>
    </div>
    <a href="php/logout.php?logout_id=<?php echo $row['id'];?>" class="logout">Logout</a>
</header>
<div class="search">
    <span class="text">Select user start chat</span>
    <input type="text" placeholder="Enter name to search...">
    <button><i class="fa fa-search"></i></button>
</div>
<div class="users-list">
  
</div>
       </section>
    </div>
    <script src="javascript/users.js"></script>
</body>

</html>