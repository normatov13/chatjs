<?php
session_start();
?>
<?php
if(isset($_SESSION['id'])){
    header("Location:User.php");
}
?>
<?php  include_once "header.php" ; ?>

<body>
    <div class="wrapper">
        <section class="form login">
            <header>Real time App</header>
            <form action="#">
                <div class="error-txt"></div>
          
                    <div class="field input">
                        <label for="ename">Email</label>
                        <input type="text" required id="email" name="email" placeholder="Email">
                    </div>
                    <div class="field input">
                        <label for="pss">Password</label>
                        <input type="password" required id="pss" name="password" placeholder="Password">
                        <i class="fa fa-eye"></i>
                    </div>
                  
                    <div class="field submit">
                        <input type="submit" value="Continue to Chat">
                    </div>
               
            </form>
            <div class="link">Not yet sign Up ? <a href="/index.php">Signup now..</a></div>
        </section>
    </div>
    <script src="javascript/pass-show-hide.js"></script>
    <script src="javascript/login.js"></script>

</body>

</html>