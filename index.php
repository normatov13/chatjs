<?php
session_start();
if(isset($_SESSION['id'])){
    header("Location:User.php");
}
?>
<?php  include_once "header.php" ; ?>

<body>
    <div class="wrapper">
        <section class="form signup">
            <header>Real time App</header>
            <form action="post" enctype="multipart/form-data">
                <div class="error-txt"></div>
                <div class="name-details">
                    <div class="field input">
                        <label for="name">First Name</label>
                        <input type="text" id="name" placeholder="Name" name="name" required>
                    </div>
                    <div class="field input">
                        <label for="lname">Last Name</label>
                        <input type="text" id="lname" placeholder="Last Name" name="lname" required>
                    </div>
                </div>
                    <div class="field input">
                        <label for="ename">Email</label>
                        <input type="text" id="ename" placeholder="Email" name="email" required>
                    </div>
                    <div class="field input">
                        <label for="pss">Password</label>
                        <input type="password" id="pss" placeholder="Password" name="password" required>
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="field image">
                        <label for="image">Image</label>
                        <input type="file" id="image" placeholder="Image" name="image" required>
                    </div>
                    <div class="field submit">
                        <input type="submit" value="Continue to Chat">
                    </div>
               
            </form>
            <div class="link">Already sign Up ? <a href="login.php">Login now..</a></div>
        </section>
    </div>
    <script src="javascript/pass-show-hide.js"></script>
    <script src="javascript/signup.js"></script>
</body>

</html>