<?php

session_start();
if(isset($_SESSION['id'])){
   include_once "config.php";
   $outgoing_id = mysqli_real_escape_string($conn,$_POST['outgoing_id']);
   $incoming_id = mysqli_real_escape_string($conn,$_POST['incoming_id']);
   $output = "";
  
   $quer = "SELECT * FROM messages 
   LEFT JOIN users ON users.id = messages.outgoing_msg_id
   WHERE (incoming_msg_id={$incoming_id} AND outgoing_msg_id={$outgoing_id}) 
   OR (incoming_msg_id={$outgoing_id} AND outgoing_msg_id={$incoming_id})";

   $sql = mysqli_query($conn,$quer);
   if(mysqli_num_rows($sql) > 0){
       while($row = mysqli_fetch_assoc($sql)){
           if($row['outgoing_msg_id']===$outgoing_id){ // Agar bu shart bajarilsa u holda bu msg yuboruvchining messagelri boladi
            $output .= ' <div class="chat outgoing">
                            <div class="details">
                                <p>'.$row['msg'].'</p>
                            </div>
                        </div>';

           }else{ // Aks holda u mehmon mim msg boldai
            $output .= '<div class="chat incoming">
                            <img src="php/images/'.$row['img'].'" alt="">
                            <div class="details">
                                <p>'.$row['msg'].'</p>
                            </div>
                        </div> ';
           }
       }
       echo $output;
   }
}else{
    header("Location:/../login.php");
}


?>